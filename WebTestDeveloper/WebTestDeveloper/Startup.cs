﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebTestDeveloper.Startup))]
namespace WebTestDeveloper
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
